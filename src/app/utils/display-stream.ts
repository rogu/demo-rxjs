import { BehaviorSubject } from 'rxjs';
import { ElementRef, ViewChild, Directive } from '@angular/core';

@Directive()
export class DisplayStreamValue {

  onProgress = new BehaviorSubject(false);
  current!: string;
  @ViewChild('content') content!: ElementRef;
  @ViewChild('description') description!: ElementRef;

  constructor() { }

  display(streamName: string, description = '') {
    this.content.nativeElement.innerHTML = '';
    this.description.nativeElement.innerHTML = description;
    this.onProgress.next(true);
    this.current = streamName;
    // 3 types of observer notifications
    return {
      next: (value: any) => {
        this.displayLine('NEXT', value);
      },
      error: (error: any) => {
        this.displayLine('ERROR', error);
        this.onProgress.next(false);
      },
      complete: () => {
        this.displayLine('COMPLETE');
        this.onProgress.next(false);
      }
    }
  }

  displayLine(type: string, val = null) {
    this.content.nativeElement
      .appendChild(document.createElement('div'))
      .textContent = `[${type}]${val !== null ? ': ' + JSON.stringify(val) : ''}`;
  }
}
