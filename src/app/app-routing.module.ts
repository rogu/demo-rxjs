import { TypeAheadWrapperComponent } from './containers/examples/components/type-ahead.component';
import { CatchDotGameComponent } from './containers/examples/components/catch-dot/catch-dot-game.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OperatorsComponent } from './containers/operators/operators';
import { CalorieCalcComponent } from './containers/examples/components/calc.component';
import { StopWatchComponent } from './containers/examples/components/stop-watch.component';
import { StreamsComponent } from './containers/streams/streams.component';
import { ExamplesComponent } from './containers/examples/examples.component';
import { InitComponent } from './core/init.component';
import { DiceComponent } from './containers/examples/components/dice.component';
import { CarRacingComponent } from './containers/examples/components/car-racing/car-racing.component';

const routes: Routes = [
  { path: "streams", component: StreamsComponent },
  { path: "operators", component: OperatorsComponent },
  {
    path: 'examples', component: ExamplesComponent, children: [
      { path: "calorie-calc", component: CalorieCalcComponent },
      { path: "catch-dot", component: CatchDotGameComponent },
      { path: "stop-watch", component: StopWatchComponent },
      { path: "dice", component: DiceComponent },
      { path: "type-ahead", component: TypeAheadWrapperComponent },
      { path: "car-racing", component: CarRacingComponent }
    ]
  },
  { path: '⌂', component: InitComponent },
  { path: '**', redirectTo: '⌂' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
