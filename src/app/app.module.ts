import { TypeAheadComponent, TypeAheadWrapperComponent } from './containers/examples/components/type-ahead.component';
import { CatchDotGameComponent } from './containers/examples/components/catch-dot/catch-dot-game.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatDividerModule } from '@angular/material/divider';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatTabsModule } from '@angular/material/tabs';

import { MatToolbarModule } from '@angular/material/toolbar';
import { HttpClientModule } from '@angular/common/http';
import { CalorieCalcComponent } from './containers/examples/components/calc.component';
import { OperatorsComponent } from './containers/operators/operators';
import { StopWatchComponent } from './containers/examples/components/stop-watch.component';
import { StreamsComponent } from './containers/streams/streams.component';
import { ExamplesComponent } from './containers/examples/examples.component';
import { InitComponent } from './core/init.component';
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { DiceComponent } from './containers/examples/components/dice.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CarRacingComponent } from './containers/examples/components/car-racing/car-racing.component';

@NgModule({
  declarations: [
    AppComponent,
    CalorieCalcComponent,
    OperatorsComponent,
    StopWatchComponent,
    StreamsComponent,
    ExamplesComponent,
    InitComponent,
    DiceComponent,
    CatchDotGameComponent,
    TypeAheadComponent,
    TypeAheadWrapperComponent,
    CarRacingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatDividerModule,
    MatButtonModule,
    MatCardModule,
    MatTabsModule,
    MatInputModule,
    MatGridListModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatChipsModule,
    MatSnackBarModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
