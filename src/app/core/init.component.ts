import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-init',
  template: `
    <h1 class="title">Szkolenie Programowanie Reaktywne - RxJs</h1>
    <p>
      Programowanie reaktywne najczęściej używane jest w kontekście zdarzeń.
      <br>
      Zdarzenia te to np. aktywność użytkownika, odpowiedź serwera, czy interwał.
      <br>
      RxJs pomaga nam pracować z tymi zdarzeniami tworząc strumienie danych,
      które możemy modyfikować (lub łączyć ze sobą) korzystając z bardzo bogatego API ReactiveX.
    </p>
    <hr  />
    <a class="mt-4" target="_blank" mat-raised-button color="primary" href="https://www.learnrxjs.io">więcej o rxjs</a>
  `,
  styles: []
})
export class InitComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
