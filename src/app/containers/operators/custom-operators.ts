
import { debounceTime, startWith, tap, withLatestFrom, map } from 'rxjs/operators';
import { Observable, Observer } from 'rxjs';

export const customDouble = (source: Observable<any>): Observable<number> =>
  new Observable((observer: Observer<number>) =>
    source.subscribe({
      next: (val) => observer.next(val * 2),
      error: (err) => observer.error(err),
      complete: () => observer.complete()
    })
  );

export const customMultiple = (val1: number) =>
  (src: Observable<any>) =>
    new Observable((observer: Observer<any>) =>
      src.subscribe({
        next: (val2) => observer.next(val1 * val2),
        error: (err) => observer.error(err),
        complete: () => observer.complete()
      })
    )

export const typeAhead = (want: Observable<any>) =>
  (src: Observable<any>) =>
    new Observable((observer: Observer<any>) =>
      want
        .pipe(
          map(({ target: { value } }) => value),
          startWith(''),
          withLatestFrom(src)
        )
        .subscribe({
          next: ([value, arr]) => {
            observer.next(arr.filter((item: string) =>
              item.toLowerCase().startsWith(value.toLowerCase())))
          },
          error: (err) => observer.error(err),
          complete: () => observer.complete()
        })
    )
