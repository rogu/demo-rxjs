import { fromEvent, range } from "rxjs";
import { filter, take, takeWhile, throttleTime, map } from "rxjs/operators";

export const filtering = (display: Function) => {
  return {
    filter() {
      range(0, 10)
        .pipe(filter((value) => value % 2 === 0))
        .subscribe(display('filter', 'filtruje dane strumienia'));
    },

    takeWhile() {
      range(0, 10)
        .pipe(takeWhile((value) => value < 5))
        .subscribe(display('takeWhile', 'kończy strumień kiedy zwraca false'));
    },

    throttle() {
      const timeInterval = 500;
      fromEvent(document, 'mousemove')
        .pipe(
          throttleTime(timeInterval),
          take(5),
          map((e: any) => ({ x: e.clientX }))
        )
        .subscribe(display('throttle', 'Ogranicza emitowanie wartości zgodnie z interwałem. Tutaj event mousemove'));
    },
  }
}
