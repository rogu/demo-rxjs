import { concatMap, delay, groupBy, mergeMap, toArray, takeUntil } from 'rxjs/operators';
import { map, switchMap, exhaustMap } from 'rxjs/operators';
import { EMPTY, from, fromEvent, iif, interval, of } from "rxjs";

export const transformationHigherOrder = (display: Function) => {
    return {
        concatMap: () => {
            const text = 'hello world!';
            from(text.split(''))
                .pipe(
                    concatMap((val) => iif(() => !!val.trim(), of(val).pipe(delay(100)), EMPTY))
                )
                .subscribe(display('concatMap', 'Mapuje wartość do wew. strumienia. Emituje horyzontalnie wg kolejności.'));
        },

        switchMap() {
            of<number[]>([1, 2, 3]).pipe(
                switchMap(() => of([0])),
                map((value) => [...value, 4])
            )
                .subscribe(display('switchMap', 'Przełącza na inny strumień. Poprzedni jest anulowany'));
        },

        exhaust() {
            const clicks$ = fromEvent(document, 'click');
            const action$ = interval(500).pipe(takeUntil(fromEvent(document, "dblclick")));
            const higherOrder$ = clicks$.pipe(exhaustMap(_ => action$));
            higherOrder$.subscribe(display('exhaust', 'Anuluje emisje zew. strumienia jeżeli wew. strumień się nie zakończył. Tutaj dblclick kończy wew. strumień.'));
        },

        groupBy() {
            const people = [
                { name: 'Sue', age: 25 },
                { name: 'Joe', age: 30 },
                { name: 'Filip', age: 30 },
                { name: 'Frank', age: 25 },
                { name: 'Sarah', age: 35 }
            ];
            // emit each person
            const source = from(people);
            // group by age
            const example = source.pipe(
                groupBy(person => person.age),
                // return each item in group as array
                mergeMap(group => group.pipe(toArray()))
            );
            example.subscribe(display('groupBy', 'Grupuje strumienie wg jakiejś wartości. Tutaj wiek'));
        }
    }
}
