import { Api } from './../../../utils/api';
import { map, delay, take } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { forkJoin, of, range, timer, merge, interval, zip, concat, combineLatest } from "rxjs";

export const combination = (display: Function, displayLine: Function) => {
  return {
    forkJoin() {
      forkJoin({
        ajax: ajax(Api.DATA_ITEMS).pipe(map(({ response: { total } }: any) => ({ total }))),
        of: of('any value'),
        range: range(0, 1).pipe(delay(2000))
      })
        .subscribe(display('forkJoin', 'Łączy wszystkie zakończone strumienie i emituje ostatnią wartość wszystkich strumieni w postaci obiektu.'));
    },

    merge() {
      const a = timer(0, 200).pipe(
        take(3),
        map((v) => `a-` + v));

      const b = timer(0, 2000).pipe(
        take(3),
        map((v) => `b-` + v));

      merge(a, b)
        .subscribe(display('merge', 'Łączy strumienie wertykalnie; kto pierwszy ten lepszy.'));
      // can also be used as an instance operator
      // a.pipe(merge(b)).subscribe(display('merge'));
    },

    concat() {
      const a = of(1, 2, 3).pipe(
        map((v) => `a-` + v));

      const b = of(10, 20, 30).pipe(
        delay(1000),
        map((v) => `b-` + v));

      concat(a, b).subscribe(display('concat', 'Łączy strumienie horyzontalnie. Po zakończeniu pierwszego przechodzi do następnego.'));
      // can also be used as an instance operator
      // a.pipe(concat(b)).subscribe(display('concat'));
    },

    zip() {
      // Complete when at least one is complete
      zip(
        range(100, 3),
        interval(1000)
      )
        .subscribe(display(
          'zip',
          'Strumienie czekają na siebie. Wartości się zazębiają. Zip kończy się w momencie zakończeniem któregokolwiek strumienia'));
    },

    combineLatest() {
      const a = of(1, 2, 3).pipe(
        map((v) => `a-` + v));

      const b = of(10, 20, 30).pipe(
        delay(1000),
        map((v) => `b-` + v));

      combineLatest([a, b]).subscribe(
        display(
          'combineLatest',
          'Zaczyna zwracać dane w chwili gdy wszystkie połączone strumienie posiadają wartość; stąd, w tym przykładzie, pominiete zostały początkowe wartości strumienia a'
        ));
    }
  }
}
