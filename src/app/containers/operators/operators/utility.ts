import { interval, of } from "rxjs"
import { finalize, take, tap } from "rxjs/operators";

export const utility = (display: Function, displayLine: Function) => {
    return {

        toPromise() {
            of('zamiana observable na promise')
                .pipe(
                    tap(() => alert('Promise nie kończy strumienia. Odśwież stronę aby uaktywnić menu.'))
                )
                .toPromise().then((val) => display('toPromise', val));
        },

        finalize() {
            interval(100)
                .pipe(
                    take(2),
                    finalize(() => displayLine('COMPLETE', 'works from finalize operator'))
                )
                .subscribe(display('finalize', 'uruchamia się kiedy strumień się kończy.'));
        },

        tap() {
            of('tralala')
                .pipe(
                    tap(console.log),
                    tap(() => displayLine('NEXT', 'works'))
                )
                .subscribe(display('tap', 'Jest transparenty; nie zmienia wartości strumienia. Często używany jako side-effect np. logowanie czy wyświetlanie w DOM.'));
        }
    }

}
