import { transformationHigherOrder } from './operators/transformation-higher-order';
import { multicasting } from './operators/multicasting';
import { utility } from './operators/utility';
import { custom } from './operators/custom';
import { combination } from './operators/combination';
import { filtering } from './operators/filtering';
import { transformation } from './operators/transformation';
import { Component } from '@angular/core';
import { DisplayStreamValue } from '../../utils/display-stream';

@Component({
  selector: 'app-operators',
  templateUrl: 'operators.html'
})
export class OperatorsComponent extends DisplayStreamValue {

  operators = {
    transformation: transformation(this.display.bind(this)),
    transformationHigherOrder: transformationHigherOrder(this.display.bind(this)),
    filtering: filtering(this.display.bind(this)),
    combination: combination(this.display.bind(this), this.displayLine.bind(this)),
    multicasting: multicasting(this.display.bind(this), this.displayLine.bind(this)),
    utility: utility(this.display.bind(this), this.displayLine.bind(this)),
    custom: custom(this.display.bind(this))
  };

  constructor() {
    super();
  }

  run(fn: Function) {
    fn();
  }
}
