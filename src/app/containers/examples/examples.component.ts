import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-examples',
  templateUrl: './examples.component.html'
})
export class ExamplesComponent implements OnInit {
  current!: string;
  examples = ['calorie-calc', 'catch-dot', 'stop-watch', 'dice', 'type-ahead', 'car-racing'];

  constructor() { }

  ngOnInit() {
  }

}
