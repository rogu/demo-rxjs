import { tap, delay } from 'rxjs/operators';
import { fromEvent } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { of } from 'rxjs';
import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { typeAhead } from '../../operators/custom-operators';

@Component({
  selector: 'type-ahead',
  styles:[`
    .brick {
      border:2px solid;
      padding: 5px;
      display: inline-block;
    }
  `],
  template: `
  <div>
    <mat-form-field appearance="fill">
        <mat-label>US State</mat-label>
        <input #wanted autofocus matInput type="text" placeholder="type us state eg. Arizona">
      </mat-form-field>
    <br>
    <span class="brick" *ngFor="let item of data$|async">{{item}}</span>
  </div>
  `
})

export class TypeAheadComponent implements AfterViewInit {
  data$: any;
  @ViewChild('wanted') wanted!: ElementRef<any>;
  @Input() data!: string[];

  ngAfterViewInit(): void {
    this.data$ = of(this.data).pipe(
      typeAhead(fromEvent(this.wanted.nativeElement, 'input')),
      delay(0)
    )
  }
}

@Component({
  template: `
    <type-ahead [data]="data"></type-ahead>
  `
})

export class TypeAheadWrapperComponent implements OnInit {
  data = ['Alabama', 'Alaska', 'American Samoa', 'Arizona', 'Arkansas', 'California', 'Colorado',
    'Connecticut', 'Delaware', 'District Of Columbia', 'Federated States Of Micronesia', 'Florida', 'Georgia',
    'Guam', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine',
    'Marshall Islands', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana',
    'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
    'Northern Mariana Islands', 'Ohio', 'Oklahoma', 'Oregon', 'Palau', 'Pennsylvania', 'Puerto Rico', 'Rhode Island',
    'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virgin Islands', 'Virginia',
    'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
  constructor() { }

  ngOnInit() { }
}
