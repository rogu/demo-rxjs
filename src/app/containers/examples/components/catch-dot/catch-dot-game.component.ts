import { NotificationService } from './../../../../core/services/notification.service';
import { Observable, timer } from 'rxjs';
import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { fromEvent } from 'rxjs';
import { scan, map, switchMap, takeWhile, finalize, share } from 'rxjs/operators';

interface State {
  score: number;
  intrvl: number;
  x: number;
  y: number;
  bgColor?: string;
}

@Component({
  selector: 'catch-dot-game',
  template: `
    <p>
      Aby rozpocząć najedź myszką na czerwoną kółko.
      <br>
      Każde najechanie myszką na pojawiające się kółko, to dodatkowy punkt dla Ciebie.
    </p>

    {{game$|async|json}}
    <div id='timer' #timer>{{(game$|async)?.intrvl}}</div>
    <div id='dot'
      [style.transform]="'translate(' + (game$|async)?.x + 'px,' + (game$|async)?.y +'px)'"
      [style.backgroundColor]="(game$|async)?.bgColor"
      #dot>
        {{(game$|async)?.score}}
    </div>
  `,
  styleUrls: ['catch-dot-game.component.scss']
})

export class CatchDotGameComponent implements AfterViewInit {

  @ViewChild('dot') dot: any;
  @ViewChild('timer') timer: any;
  game$!: Observable<State>;
  initState: State = { score: 0, intrvl: 500, x: 1, y: 1 };
  random = () => Math.random() * 300 >> 0;

  constructor(
    private notification: NotificationService
  ){}

  makeInterval(val: State) {
    return timer(0, val.intrvl)
      .pipe(
        map(v => ({ ...val, intrvl: 5 - v }))
      )
  }

  nextState(acc: State): State {
    return {
      x: this.random(),
      y: this.random(),
      score: (acc.score += 1),
      intrvl: (acc.intrvl -= 15),
      bgColor: '#' + ((Math.random() * 0xffffff) << 0).toString(16)
    }
  }

  isNotGameOver(val: State) {
    return val.intrvl >= 0;
  }

  ngAfterViewInit(): void {
    this.game$ = fromEvent<Event>(this.dot.nativeElement, 'mouseover').pipe(
      scan<Event, State>(this.nextState.bind(this), this.initState),
      switchMap(this.makeInterval.bind(this)),
      takeWhile(this.isNotGameOver),
      finalize(() => this.notification.showInfo('ouch!')),
      share()
    );
    this.game$.subscribe();
  }
}

