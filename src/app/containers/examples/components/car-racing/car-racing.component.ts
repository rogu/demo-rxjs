import { Game, Car, Road, Player, Types } from '.././models';
import { finalize, startWith, switchMap, scan, tap, share, map, takeWhile } from 'rxjs/operators';
import { BehaviorSubject, of, combineLatest, fromEvent, interval, Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './car-racing.component.html',
  styleUrls: ['./car-racing.component.css']
})

export class CarRacingComponent implements OnInit {
  game$!: Observable<Game>;
  gameHeight = 10;
  gameWidth = 6;
  levelDuration = 500;
  gameSpeed$ = new BehaviorSubject(200);

  updateScore = (acc: Game, [, road, player]: Game): Game => {
    const shouldUpdate = road.cars[0].y !== player.x && road.cars[0].y === this.gameHeight - 1;
    const update: { score?: number } = { ...shouldUpdate && { score: acc[0].score + 1 } };
    return [{ ...acc[0], ...update }, road, player];
  }

  updateLives = (acc: Game, [state, road, player]: Game): Game => {
    const shouldUpdate = road.cars[0].y === this.gameHeight - 1 && road.cars[0].x === player.x;
    const update: { lives: number } = { lives: shouldUpdate ? acc[0].lives - 1 : acc[0].lives };
    return [{ ...state, ...update }, road, player];
  }

  updateLevel = (acc: Game, [state, road, player]: Game): Game => {
    const shouldUpdate = acc[0].duration <= 0;
    const update: { duration: number, level: number } = {
      ...shouldUpdate
        ? {
          duration: this.levelDuration * (acc[0].level + 1),
          level: acc[0].level + 1
        }
        : {
          duration: acc[0].duration - 10,
          level: acc[0].level
        }
    }
    return [{ ...state, ...update }, road, player];
  };

  updateFrame = ([state, road, playerPosition]: Game) => {
    const frame = state.frame.map((row: any[], idRow): any => {
      const carInRow = road.cars.find((el): any => idRow === el.y);
      const isLastRow = idRow === state.frame.length - 1;
      const nextRowState = carInRow
        ? row.map((cValue, cId): any => carInRow.x === cId ? Types.Car : cValue)
        : [...row];
      isLastRow && (nextRowState[playerPosition.x] = Types.Player);
      return nextRowState;
    })
    const nextState = { ...state, frame };
    return [nextState, road, playerPosition] as Game
  }

  renderGameOver = () => console.log('game over');
  updateSpeed = ([state]: Game) => state.duration <= 0 && this.gameSpeed$.next(Math.max(this.gameSpeed$.value - 50, 50));
  isNotGameOver = ([state]: Game) => state.lives > 0;

  ngOnInit() {

    const car = (x: number, y: number): Car => ({ x, y });
    const randomCar = (): Car => car(Math.floor(Math.random() * this.gameWidth), 0);

    const road$ = this.gameSpeed$.pipe(
      switchMap(i =>
        interval(i).pipe(
          scan((road: Road): Road => {
            road.cars = road.cars.filter(c => c.y < this.gameHeight - 1);
            road.cars.forEach(c => c.y++);
            (road.cars[0].y === this.gameHeight / 2) && road.cars.push(randomCar());
            return road;
          }, { cars: [randomCar()] }
          )
        )
      )
    );

    const keys$ = fromEvent<KeyboardEvent>(document, 'keyup').pipe(
      startWith({ code: '' }),
      map(({ code }): number => {
        const keys: any = { ArrowLeft: -1, ArrowRight: 1 };
        return keys[code];
      })
    );

    const player$ = keys$.pipe(
      scan((player: Player, key: number): Player => {
        return {
          ...player,
          ...{
            x: key === 1 && player.x < this.gameWidth - 1 || key === -1 && player.x > 0
              ? player.x + key
              : player.x
          },
        }
      }, { x: 0 })
    );

    const state$ = of({
      score: 0,
      lives: 3,
      level: 1,
      duration: this.levelDuration,
      frame: Array(this.gameHeight).fill(0).map(e => Array(this.gameWidth).fill(0))
    });

    this.game$ = combineLatest([state$, road$, player$]).pipe(
      scan(this.updateScore),
      scan(this.updateLives),
      scan(this.updateLevel),
      map(this.updateFrame),
      tap(this.updateSpeed),
      takeWhile(this.isNotGameOver, true),
      finalize(this.renderGameOver),
      share()
    );
  }
}
