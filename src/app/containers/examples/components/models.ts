
export interface Car {
  x: number;
  y: number;
}

export enum Types {
  Car = 1,
  Player = 2
}

export interface Road {
  cars: Car[];
}

export interface State {
  score: number;
  lives: number;
  level: number;
  duration: number;
  frame: number[][]
}

export interface Player {
  x: number;
}
export type Game = [State, Road, Player];
