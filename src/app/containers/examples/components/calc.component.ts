import { Component, AfterViewInit, ViewChild } from "@angular/core";
import { combineLatest, fromEvent, iif, Observable, of } from "rxjs";
import { map, mergeMap } from "rxjs/operators";

@Component({
  selector: "app-calc",
  template: `
    <h3 class="title">Prosty kalkulator kalorii</h3>
    <h4>Przykład pokazuje zastosowanie operatora combineLatest, mergeMap, iif</h4>

      <mat-form-field appearance="fill">
        <mat-label>your weight (number)</mat-label>
        <input matInput #weight>
      </mat-form-field>
      <span> * 24 * </span>
      <mat-form-field appearance="fill">
        <mat-label>your activity (</mat-label>
        <input matInput #activity>
      </mat-form-field>

    <ng-template #ok let-data>
        <div style="color: lightgreen">
            You should eat <b>{{data?.value}}</b> calories to maintain weight.
        </div>
    </ng-template>
    <ng-template #fail let-data>
        <div style="color: coral;">{{data?.error}}</div>
    </ng-template>
    <ng-container *ngTemplateOutlet="(result$|async)?.value ? ok : fail; context: {$implicit: result$|async}"></ng-container>
    `,
})

export class CalorieCalcComponent implements AfterViewInit {
  @ViewChild('weight') weight: any;
  @ViewChild('activity') activity: any;
  @ViewChild('output') output: any;
  result$!: Observable<{ value?: any, error?: any }>;

  ngAfterViewInit() {

    let events$ = combineLatest([
      fromEvent<Event>(this.weight.nativeElement, 'input'),
      fromEvent<Event>(this.activity.nativeElement, 'input')
    ]).pipe(
      map(([w, a]: Event[]) => {
        return {
          weight: +(w.target as HTMLInputElement).value,
          activity: +(a.target as HTMLInputElement).value
        };
      })
    )

    this.result$ = events$
      .pipe(
        mergeMap((obj) => iif(() => obj.weight > 0 && obj.activity >= 1 && obj.activity <= 1.6,
          of(obj).pipe(map(obj => ({ value: (obj.weight * 24 * obj.activity).toFixed() }))),
          of({ error: 'błędne dane wejściowe. Waga > 1; aktywność w zakresie 1 - 1.6' }))
        )
      );
  }
}
